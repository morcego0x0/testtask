#ifndef INTERVAL_H
#define INTERVAL_H

#include <string>
#include <time.h>
namespace PVStore
{
    enum TimeMeasure {/*Year, Months, Weeks,  Hours, Minutes,*/ Seconds, Microseconds};

    class Interval
    {
    public:
        /**
         * @brief Interval allows to create time interval
         * @param value the value of the time
         * @param measure allows to determine what kind of time messure is used
         */
        Interval(int value, TimeMeasure measure);

        /**
         * @brief getSec
         * @return time in seconds
         */
        int getSec() const;

        /**
         * @brief getMs
         * @return time in microseconds
         */
        int getMs() const;

        /**
         * @brief ToStringAgo
         * @return
         */
        std::string ToStringAgo();

        /**
         * @brief operator * - allows to multiply time Intervals.
         * @param other - the Interval to multiply with.
         * @return result is always in Microseconds for simplicity
         */
        Interval operator* (Interval other);

        /**
         * @brief operator + - allows to add time Intervals.
         * @param other - the Interval to add with.
         * @return result is always in Microseconds for simplicity
         */
        Interval operator+ (Interval valueToAdd);

        /**
         * @brief operator - allows to subtract time Intervals.
         *
         * Note: if right operand greate than left the error occurs
         *
         * @param other - the Interval to subtract with.
         * @return result is always in Microseconds for simplicity
         */
        Interval operator- (Interval valueToAdd);

        /**
         * @brief operator == - allows to compare time Intervals.
         * @param other - the Interval to compare with.
         * @return true if equals
         */
        bool operator== (const Interval& other);

        /**
         * @brief operator > - allows to compare time Intervals.
         * @param other - the Interval to compare with.
         * @return true if greate;
         */
        bool operator> (const Interval& other);

        /**
         * @brief operator < - allows to compare time Intervals.
         * @param other - the Interval to compare with.
         * @return true if less;
         */
        bool operator< (const Interval& other);

        /**
         * @brief operator >= - allows to compare time Intervals.
         * @param other - the Interval to compare with.
         * @return true if operand greate or equal;
         */
        bool operator>= (const Interval& other);

        /**
         * @brief operator <= - allows to compare time Intervals.
         * @param other - the Interval to compare with.
         * @return true if operand less or equal;
         */
        bool operator<= (const Interval& other);

        std::string ToString(char thousandSeperator, int numDecimalPlaces);

    private:
        int m_Value;
        TimeMeasure m_Measure;
    };
}
#endif // INTERVAL_H
