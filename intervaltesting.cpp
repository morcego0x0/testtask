#include "intervaltesting.h"
#include <iostream>

IntervalTesting::IntervalTesting()
{
}

void IntervalTesting::RunTest()
{
    TestAdd();
    TestSub();
    TestMul();
    TestEqual();
    TestLess();
    TestGreat();
}

void IntervalTesting::TestAdd()
{
    PVStore::Interval res = PVStore::Interval(1000, PVStore::Microseconds) + PVStore::Interval(5, PVStore::Seconds);
    if( res.getSec() == 6) {
        std::cout << "TestAdd ....ok"<<std::endl;
        return;
    }
    std::cout << "TestAdd ....failed"<<std::endl;
}

void IntervalTesting::TestSub()
{
    PVStore::Interval res = PVStore::Interval(10, PVStore::Seconds) - PVStore::Interval(5, PVStore::Seconds);
    if( res.getSec() == 5) {
        std::cout << "TestSub ....ok"<<std::endl;
        return;
    }
    std::cout << "TestSub ....failed"<<std::endl;
}

void IntervalTesting::TestMul()
{
    PVStore::Interval res = PVStore::Interval(10, PVStore::Seconds) * PVStore::Interval(5, PVStore::Seconds);
    if( res.getSec() == 50) {
        std::cout << "TestMul ....ok"<<std::endl;
        return;
    }
    std::cout << "TestMul ....failed"<<std::endl;
}

void IntervalTesting::TestEqual()
{
    if(PVStore::Interval(5, PVStore::Seconds) == PVStore::Interval(5, PVStore::Seconds)) {
        std::cout << "TestEqual ....ok"<<std::endl;
        return;
    }
    std::cout << "TestEqual ....failed"<<std::endl;
}

void IntervalTesting::TestLess()
{
    if(PVStore::Interval(4, PVStore::Seconds) < PVStore::Interval(5, PVStore::Seconds)) {
        std::cout << "TestLess ....ok"<<std::endl;
        return;
    }
    std::cout << "TestLess ....failed"<<std::endl;
}

void IntervalTesting::TestGreat()
{
    if(PVStore::Interval(6, PVStore::Seconds) > PVStore::Interval(5, PVStore::Seconds)) {
        std::cout << "TestGreat ....ok"<<std::endl;
        return;
    }
    std::cout << "TestGreat ....failed"<<std::endl;
}
