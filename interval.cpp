#include "interval.h"

#include <string.h>
#include <stdexcept>

namespace PVStore {

    Interval::Interval(int value, TimeMeasure measure)
        : m_Value(value)
        , m_Measure(measure)
    {}

    int Interval::getSec() const
    {
        if (Microseconds == m_Measure)
           return m_Value / 1000;

        return m_Value;
    }

    int Interval::getMs() const
    {
        if (Seconds == m_Measure)
            return m_Value * 1000;

        return m_Value;
    }

    std::string Interval::ToStringAgo()
    {
        return std::string();
    }

    Interval Interval::operator *(Interval other)
    {
        //since we multiply in seconds we have to divide
        //result by 1000 to obtain proper results
        m_Value = (this->getMs() * other.getMs()) / 1000;

        return Interval(m_Value, Microseconds);
    }

    Interval Interval::operator+ (Interval valueToAdd)
    {
        m_Value = this->getMs() + valueToAdd.getMs();
        return Interval(m_Value, Microseconds);
    }

    Interval Interval::operator -(Interval valueToAdd)
    {
        if (*this > valueToAdd)
        {
            m_Value = this->getMs() - valueToAdd.getMs();
        }
        else
        {
            throw std::logic_error("righ opernad should be less");
        }
        return Interval (m_Value, Microseconds);
    }

    bool Interval::operator ==(const Interval& other)
    {
        if (this->m_Measure == other.m_Measure)
            if (this->m_Value == other.m_Value)
                return true;

        return false;
    }

    bool Interval::operator >(const Interval &other)
    {
        if (*this == other)
            return false;

        return this->getMs() - other.getMs() > 0;
    }

    bool Interval::operator <(const Interval &other)
    {
        if (*this == other || *this > other)
            return false;

        return true;
    }

    bool Interval::operator >=(const Interval &other)
    {
        return (*this == other || *this > other);
    }

    bool Interval::operator <=(const Interval &other)
    {
        return (*this == other || *this < other);
    }

    std::string Interval::ToString(char thousandSeperator = ',', int numDecimalPlaces = 5)
    {
        return std::string();
    }
}
