TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    interval.cpp \
    intervaltesting.cpp

HEADERS += \
    interval.h \
    intervaltesting.h

