#ifndef INTERVALTESTING_H
#define INTERVALTESTING_H

#include "interval.h"

class IntervalTesting
{
public:
    IntervalTesting();

    void RunTest();
private:
    void TestAdd();
    void TestSub();
    void TestMul();
    void TestEqual();
    void TestLess();
    void TestGreat();
};

#endif // INTERVALTESTING_H
